---
bookCollapseSection: true
weight: 9
title: "App Management"
---

# Managing Applications
Application management section takes you through on How to Publish an App, manage its Lifecycle and new releases. There is also a section on Google Enterprise App Management guiding you through on how to enroll a device as a Google enterprise enabled work profile. 

How to Install and Uninstall an App is also covered within this section. 

Descriptive procedures on Managing Applications are explained as below:

<div>
    <ul style="list-style-type:disc;">
<li><a href="publish-an-app">Publish an App</a></li>
<li><a href="releases-and-manage-life-cycle">Releases and Life Cycle Management</a></li>
<li><a href="google-enterprise-app-management">Google Enterprise App Management</a></li>
<li><a href="install-and-uninstall-app">Install and Uninstall App</a></li>
</ul>
</div>