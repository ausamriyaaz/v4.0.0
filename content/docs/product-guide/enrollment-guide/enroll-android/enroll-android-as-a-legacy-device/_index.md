---
bookCollapseSection: true
weight: 6
---

# Android Device Enrollment as a Legacy Device

Legacy enrollment allows you to manage settings and apps on the device. This type of enrollment offers wider control over the device allowing features such as the ones indicated below:

<ul style="list-style-type:disc;">
<li>Factory Reset Protection (FRP) - for managing devices and for recovering in the event of employee leaving.</li>
<li>Reset device passwords - secure this feature on encrypted devices.</li>
<li>Block removal of the device administrator.</li>
<li>Admin controlled passcodes - that enable locking the user out of a device.</li>
</ul>
