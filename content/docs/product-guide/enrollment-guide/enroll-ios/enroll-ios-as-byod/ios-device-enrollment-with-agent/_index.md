---
bookCollapseSection: true
weight: 4
---

# iOS Device enrollment(BYOD) with agent

{{< hint info >}}
<strong>Pre-requisites</strong>
<ul style="list-style-type:disc;">
    <li>Server is <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a></li>
    <li>Logged into the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">device mgt portal</a></li>
    <li>Click on Add device (https://{IP}:{port}/devicemgt/device/enroll)</li>
    <li>Click on iOS from "DEVICE TYPES"</li>
    <li>Scan the QR code that appear with a QR code scanning app or type https://IP:port/ios-web-agent/enrollment in safari browser.</li>
</ul>
{{< /hint >}}



<strong>If the device is above iOS 12.2</strong>(If you have installed OS updates after March 2019)

<iframe width="560" height="315" src="https://www.youtube.com/embed/yyR62HLQtew" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>


<strong>If the device is bellow iOS 12.2</strong>

<iframe width="560" height="315" src="https://www.youtube.com/embed/WHwSU9p5d90" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>