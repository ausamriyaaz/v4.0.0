---
bookCollapseSection: true
weight: 2
title: "About This Release"
---
# About This Release

The current version of Entgra IoT Server is 4.0.0. 

As the 17<sup>th</sup> installment in succession, this version robustly demonstrates a 100% performance boost. 
This is also the richest in terms of features and improvements for all supported Android, iOS, macOS and Windows devices.

Some of the highlights of this release include:

<ul style="list-style-type:disc;">

<li><strong>Google Enterprise EMM Partner for Android</strong> 

Recognized as one of the best _<a target=”_blank” href="https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby">Android Device and Service Providers by Google</a>_, this means that all public apps from Google Playstore can now be installed silently. Combining this with the enrollment time app installation policy, you can now define some private/public apps to be installed silently from Playstore when the user enrols a device.</li>

<li><strong>Pre-Whitelist Devices and Auto-Assign to Groups </strong>

Strengthening security a step further, the Entgra IoT Server now enables command-line based enrollment for Android. Prior to enrollment, it is possible to whitelist which devices are allowed to be enrolled with the server. This can be done based on the serial numbers of the devices. Also at the time of enrollment, the device can be automatically assigned to the groups based on predefined rules. </li>

<li><strong>Powerful Reporting UI Capabilities with Real-Time Event Processing</strong>

With  <a target=”_blank” href="https://wso2.com/">WSO2</a> Siddhi Core Integration, Entgra IoT server now has a new reporting UI that carries a set of prebuilt reports useful for an EMM administration. This release also carries a new <a target=”_blank” href="https://siddhi-io.github.io/siddhi/">siddhi core-based</a> implementation to do real-time complex event processing and also an in-house solution for batch processing to create customized reports as you wish.
</li>
</ul>

## What's New in This Release

[Entgra IoT Server](https://entgra.io/) version 4.0.0 is the successor of <a target="_blank" href="https://entgra-documentation.gitlab.io/v3.8.0/">Entgra IoT Server 3.8.0</a>. 

Some prominent features and enhancements that this release offers are
 as follows:

### <strong>Android Devices</strong>

<table cellpadding="1">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2">Feature</font></th></tr></thead><tbody>
<tr><td><font style="Tahoma" size="2">Google enterprise integration</td></tr>
<tr><td><font style="Tahoma" size="2">Remote screen keyboard and mouse inputs while on the screen-sharing mode</td></tr>
<tr><td><font style="Tahoma" size="2">App screen usage time policy to track/restrict screen usage per app</td></tr>
<tr><td><font style="Tahoma" size="2">Whitelist/Blacklist connected peripherals plugged-in</td></tr>
<tr><td><font style="Tahoma" size="2">Force devices to be locked on to a given WiFi network</td></tr>
<tr><td><font style="Tahoma" size="2">Introduction of Entgra secure browser application with remote settings</td></tr>
<tr><td><font style="Tahoma" size="2">Device location history view to track the fleet’s history</td></tr>
<tr><td><font style="Tahoma" size="2">Lock device when passcode fail attempts exceeded the limit</td></tr>
<tr><td><font style="Tahoma" size="2">Notification displaying for Kiosk mode</td></tr>
<tr><td><font style="Tahoma" size="2">Display a custom message when the device is locked and for locked settings</td></tr>
<tr><td><font style="Tahoma" size="2">Offline unenrollement via a special per device admin pin code</td></tr>
<tr><td><font style="Tahoma" size="2">Disallow airplane mode restriction</td></tr>
<tr><td><font style="Tahoma" size="2">Enroll COPE device using mobile data after a factory reset</td></tr>
</table>

### <strong>General Features</strong>

<table><tr><thead><td bgcolor="#9bc9f1"><strong>Feature</strong></td></thead></tr>

<tr><td><font style="Tahoma" size="2">Enroll only whitelisted serial numbers or based on any property</td></tr>
<tr><td><font style="Tahoma" size="2">Auto-assign devices to the group upon enrollment</td></tr>
<tr><td><font style="Tahoma" size="2">Over 100% server performance improvement</td></tr>
<tr><td><font style="Tahoma" size="2">Enroll only whitelisted serial numbers or based on any property</td></tr>
<tr><td><font style="Tahoma" size="2">Auto-assign devices to the group upon enrollment</td></tr>
<tr><td><font style="Tahoma" size="2">Over 100% server performance improvement</td></tr>
<tr><td><font style="Tahoma" size="2">New complex event management rule engine powered by siddhi core</td></tr>
<tr><td><font style="Tahoma" size="2">New batch event processing capability</td></tr>
<tr><td><font style="Tahoma" size="2">Bulk device enrollment via the new command-line tool</td></tr>
</table>

### <strong>macOS Features</strong>

<table><tr><thead><td bgcolor="#9bc9f1"><strong>Feature</strong></td></thead></tr>

<tr><td><font style="Tahoma" size="2">Firewall Policy</td></tr>
</table>

### <strong>iOS Features</strong>

<table><tr><thead><td bgcolor="#9bc9f1"><strong>Feature</strong></td></thead></tr>

<tr><td><font style="Tahoma" size="2">External profile add and remove APIs</td></tr>
<tr><td><font style="Tahoma" size="2">Fetch security information of devices</td></tr>
</table>

### <strong>UI Features</strong>

<table><tr><thead><td bgcolor="#9bc9f1"><strong>Feature</strong></td></thead></tr>

<tr><td><font style="Tahoma" size="2">Ability to manually edit operation status</td></tr>
<tr><td><font style="Tahoma" size="2">Beta release of a new device management portal under /entgra</td></tr>
</table>

### <strong>Reporting Functionality</strong>

<table><tr><thead><td bgcolor="#9bc9f1"><strong>Feature</strong></td></thead></tr>

<tr><td><font style="Tahoma" size="2">Report for device app usage</td></tr>
<tr><td><font style="Tahoma" size="2">Report to retrieve available OS updates info of Apple devices</td></tr>
<tr><td><font style="Tahoma" size="2">Report for tracking devices that do not have a mandatory application installed</td></tr>
<tr><td><font style="Tahoma" size="2">Report for SIM changed details</td></tr>
<tr><td><font style="Tahoma" size="2">Enrollments vs Unenrollments Report</td></tr>
<tr><td><font style="Tahoma" size="2">Device status report</td></tr>
<tr><td><font style="Tahoma" size="2">Device type report</td></tr>
</table>