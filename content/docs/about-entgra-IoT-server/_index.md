---
bookCollapseSection: true
weight: 1
title: "About Entgra IoT Server"
---
# About Entgra IoT Server

This section briefly describes the architecture of the Entgra IoT server.

Entgra’s IoT Solution is leading the way in harnessing the best of  IoT potential and empowering solutions that are sought after for a variety of rapidly changing requirements in IoT. Powered by <a target=_blank href="https://wso2.com/">WSO2</a><sup><strong>*1</strong></sup> technology, built on extensible plug-in architecture and enhanced by world-class engineering expertise, Entgra IoT server is capable of empowering the best of Enterprise Mobility Management (EMM) services available today.

Based on the need for enterprises to manage their mobile and IoT devices, developed to  industry standard REST APIs, the present platform has evolved through customer requirements and feedback to what it is today. Entgra is the only open source vendor as at now to support all features for Android and iOS in the same platform, along with IoT capabilities. 

Fully accomplished and on par with the market leaders of EMM (Enterprise Mobility Management) suites, the Entgra IoT server constitutes of the essential requirements of an ideal EMM suite as specified by the _Gartner Magic Quadrant 2019<sup><strong>*2</strong></sup>_ which are complete MDM, MAM, Containment, Mobile Identity and UEM capabilities. 

Some of the core features of Entgra IoT that distinctly makes it stand out are:

<ul style="list-style-type:disc;">
 
 <li><strong>Security:</strong> of both devices and data.</li>
 
  <li><strong>Scalability:</strong> Ability to handle any number of devices and scale-up</li>
 
  <li><strong>Extensibility:</strong> Plug-in architecture</li>
 
  <li><strong>Multi-Tenancy Architecture</strong></li>
 
  <li><strong>Open Source:</strong> Freely available and flexible for commercializing </li>
 
 <li><strong>Comprehensive Unified Endpoint Management</strong> (UEM) capabilities</li>
 
  <li><strong>Google Enterprise EMM Partner<sup>*3</sup></strong> for Android</li>
  
  </ul>
 
Entgra IoT server comprises of the following broad areas of management:
 
 <strong>EMM</strong> - Enterprise Mobility Management
  
 <strong>MDM</strong> - Mobile Device Management
 
 <strong>MAM</strong> - Mobile Application Management
 
## Enterprise Mobility Management  (EMM)
 
 The EMM suite of Entgra’s IoT server comprises of extensive Device Management, Policy Management and Certificate Management capabilities. 

<table align="center">
<tr><td align="center">
 <img src= "../image/3001.jpg">

 </td></tr>
 </table>
 
 ## MDM
 
 
 Resolving complex field force management scenarios with simple, time and cost effective solutions, Entgra’s MDM is easily customizable as per the requirements. 
<ul style="list-style-type:disc;">
<li>Handles Complete MDM Lifecycle </li>
<li>Focuses on monitoring, controlling, securing and enforcing policies on devices </li>
<li>Implementation of comprehensive and customized policy management solutions </li>
 </ul>
 
## MAM
 <ul style="list-style-type:disc;">
<li>Mobile Application Management (MAM) includes application lifecycle management and securely deploying applications on user devices.</li>
<li>Application store provides the capabilities to install/uninstall or upgrade applications on user’s devices.</li>
<li>Allows the management of corporate apps well as public apps from Google Playstore or Apple app store.</li>
<li>App management includes managing licenses, bulk license provisioning and scheduled app management.</li>
 </ul>
 
## Functionality
 
 The functionality of the Entgra IoT server can be broadly classified into Core, Extended and Analytics capabilities as follows:
 
 **Core:** 
 
 Included in the IoT core functionality are its extensive device management capabilities covering all aspects of policy/configuration/operation and user management sections. This in effect is centralized around device management focusing on device plugins, event stream management and more.
 
 **Extended:**
 
 Entgra IoT server can be extended to be used with the integration, machine learning, workflows and many other areas.  
 
 **Analytics:**
 
 Extensive real-time, batch and edge analytics capabilities are provided by the Entgra IoT. 

## Entgra’s Edge - Our Potency

 <ul style="list-style-type:disc;">
<li> Fully integrated IoT security, based on <a target="_blank" href="https://wso2.com/">WSO2</a> platform, IS for Identity Federation. </li>
<li>Event Processing - Real-time, batch and edge analytics capability with real-time event processor. </li> 
<li>Open source platform with full flexibility to commercialize individual projects.  </li>
<li>OEM Partnership to embed WSO2 Technologies.  </li>
<li>Cloud and on-premise deployments, easily migratable from one to another. </li>
<li>Ultimate adaptability -  a multitude of control options and extension points.  </li> 
<li>Enterprise Grade Architecture  </li>
<li>Battle-tested platform in over global 2000 companies worldwide, handling billions of mission-critical transactions every day.  </li>
<li>Only vendor with out-of-the-box support solutions for Android, iOS and Windows in the same platform.  </li>
</ul>


## Release Version:

The current release version of Entgra IoT server is 4.0.0. 

<strong>Related Links:</strong>

 <ul style="list-style-type:disc;">
<li> <a target="_blank" href="https://docs.google.com/presentation/d/18zd0_cF2LjwVEs8m0rFq2Iy1ms5hHZcKEli23GTZUiY/edit?usp=sharing"> Presentation on Entgra EMM</a> </li>
</ul>

<strong>Notes/References:</strong>

 <ul style="list-style-type:disc;">
<li> *1 <a target="_blank" href="wso2.com/">WSO2</a> - The Technology Partner for and the designated reseller of Entgra.</li>
<li> *2 Essential EMM Suite capabilities as identified by Gartner’s Magic Quadrant for year 2019. These include MDM, MAM, MI (Mobile Identity), Mobile Content Management (MCM), Containment and EUM, all of which are supported by Entgra IoT server.</li>
-  <a target="_blank" href="https://www.gartner.com/en/documents/3340517/magic-quadrant-for-enterprise-mobility-management-suites"> Magic Quadrant for Enterprise Mobility Management Suites </a> <br>
- <a target="_blank" href="https://www.cmswire.com/mobile-enterprise/gartner-names-4-leaders-in-enterprise-mobility-management-mq/"> Gartner Names 4 Leaders in Enterprise Mobility Management MQ </a>
<li> *3 Entgra is recognized as one of the best <em><a target=”_blank” href="https://androidenterprisepartners.withgoogle.com/provider/#!/nO0FRKVachIRfVcd1gby">Android Device and Service Providers by Google</a></em>
</ul>
