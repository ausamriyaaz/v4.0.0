---
bookCollapseSection: true
weight: 3
---

# Windows Features

## Supported Operations

Entgra IoT Server facilitates one time <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Operations</a> that can be performed remotely via the Device Management Console.

The following operations can be executed on a Windows device.

<table border="1" class="tableizer-table">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2">Feature</font></th><th><font style="Arial" size="2">Description</font></th></tr></thead><tbody>
 <tr><td><font style="Arial" size="2">Get Device Information</font></td><td><font style="Arial" size="2">Fetch the device's runtime information.</font></td></tr>
 <tr><td><font style="Arial" size="2">Get Installed Applications</font></td><td><font style="Arial" size="2">Fetch the device's current location</font></td></tr>
 <tr><td><font style="Arial" size="2">Wipe Device(factory reset)</font></td><td><font style="Arial" size="2">Wipe the entreprise portion of the device</font></td></tr>
 <tr><td><font style="Arial" size="2">Lock Device</font></td><td><font style="Arial" size="2">Lock the device remotely. Similar to pressing the power button on the device and locking it.</font></td></tr>
 <tr><td><font style="Arial" size="2">Ring Device</font></td><td><font style="Arial" size="2">Ring the device for the purpose of locating the device in case of misplace.</font></td></tr>
 <tr><td><font style="Arial" size="2">Clear Passcode</font></td><td><font style="Arial" size="2">Changes the device's currently set lock code. From Android N upwards, clear passcode will not work</font></td></tr>
</tbody></table>

## Policies

The following <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Policies</a> can be executed on a Windows device.

<table border="1" class="tableizer-table">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2">Feature</font></th><th><font style="Arial" size="2">Description</font></th></tr></thead><tbody>
 <tr><td><font style="Arial" size="2">Passcode policy</font></td><td><font style="Arial" size="2">Define a password policy for the devices.</font></td></tr>
 <tr><td><font style="Arial" size="2">Encrypt storage</font></td><td><font style="Arial" size="2">Encrypt data on the device, when the device is locked and make it readable when the passcode is entered.</font></td></tr>
 <tr><td><font style="Arial" size="2">WiFi Policy</font></td><td><font style="Arial" size="2">Configure settings for accessing wireless networks.</font></td></tr>
 <tr><td><font style="Arial" size="2">Kiosk Policy</font></td><td><font style="Arial" size="2">Set up Windows OS to allow only one application to run. </font></td></tr>
</tbody></table>

### Restriction Policy

Restrictions policies are those that can be applied on a device restricting or controlling the use of certain specific device features. 

The following restriction policies are applicable on a Windows device. 

<table border="1" class="tableizer-table">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2">Feature</th><th><font style="Arial" size="2">Description</th></tr></thead><tbody>
 <tr><td><font style="Arial" size="2">System/AllowLocation</td><td><font style="Arial" size="2">Disable or enable location services</td></tr>
 <tr><td><font style="Arial" size="2">System/AllowUserToResetPhone</td><td><font style="Arial" size="2">Control acces to factory reset</td></tr>
 <tr><td><font style="Arial" size="2">DisableOneDriveFileSync</td><td><font style="Arial" size="2">Disable or enable syncing files with One drive storage</td></tr>
 <tr><td><font style="Arial" size="2">System/DisableSystemRestore</td><td><font style="Arial" size="2">Disable or enable system restore capabilities</td></tr>
 <tr><td><font style="Arial" size="2">System/AllowStorageCard</td><td><font style="Arial" size="2">Restrict access to external storages such as SD cards or USB</td></tr>
 <tr><td><font style="Arial" size="2">Security/AllowManualRootCertificateInstallation</td><td><font style="Arial" size="2">Allow installing root certificates and intermidiate certificates</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowBluetooth</td><td><font style="Arial" size="2">Disable or enable Bluetooth</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowCellularData</td><td><font style="Arial" size="2">Disable or enable mobile data</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowCellularDataRoaming</td><td><font style="Arial" size="2">Disable or enable data roaming</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowConnectedDevices</td><td><font style="Arial" size="2">Disable or enable Connected Devices Platform (CDP) which allows to discover connected device</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowNFC</td><td><font style="Arial" size="2">Disable or enable NFC beams</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowPhonePCLinking</td><td><font style="Arial" size="2">Disable or enable the ability to connect to perform a continious task with a link between phone and PC</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowUSBConnection</td><td><font style="Arial" size="2">Disable or enable USB connection</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowVPNOverCellular</td><td><font style="Arial" size="2">Decides if a VPN can be created over mobile data</td></tr>
 <tr><td><font style="Arial" size="2">Connectivity/AllowVPNRoamingOverCellular</td><td><font style="Arial" size="2">Decides if a VPN can be created over mobile data while roaming</td></tr>
 <tr><td><font style="Arial" size="2">Disable adding non-microsoft accounts</td><td><font style="Arial" size="2">Disable adding non Microsoft based account to the device</td></tr>
 <tr><td><font style="Arial" size="2">disable private browsing</td><td><font style="Arial" size="2">Decides if private browsing is allowed on the device </td></tr>
 <tr><td><font style="Arial" size="2">disable removable drive indexing</td><td><font style="Arial" size="2">Decides if the search results contain files from removable devices</td></tr>
 <tr><td><font style="Arial" size="2">disable language</td><td><font style="Arial" size="2">Disables language settings</td></tr>
 <tr><td><font style="Arial" size="2">disable cortana </td><td><font style="Arial" size="2">Decides if cortana is allowed on the device</td></tr>
 <tr><td><font style="Arial" size="2">disable region</td><td><font style="Arial" size="2">Disables region settings</td></tr>
 <tr><td><font style="Arial" size="2">disble date and time</td><td><font style="Arial" size="2">Disable changing date and time settings </td></tr>
 <tr><td><font style="Arial" size="2">assigned access</td><td><font style="Arial" size="2">Lock a user to a single application</td></tr>
</tbody></table>

