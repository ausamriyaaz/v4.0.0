---
bookCollapseSection: true
weight: 1
---

# Android Features


## Supported Operations

Entgra IoT Server facilitates one time <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Operations</a> that can be performed remotely via the Device Management Console. These operations are useful for runtime maintenance of devices.

The type of operations available for Android devices and are applicable for each enrollment type is summed up as per the table below.


<table border="1" class="tableizer-table">
<thead bgcolor="#9bc9f1"><tr class="tableizer-firstrow"><th><font style="Arial" size="2"> Feature - Description</font></th><th><font style="Arial" size="2">Legacy</font></th><th><font style="Arial" size="2">Work Profile</font></th></th><th><font style="Arial" size="2">Dedicated (Kiosk)</font></th></th><th><font style="Arial" size="2">Fully Managed (COPE)</font></th><th><font style="Arial" size="2">System App</font></th></tr></thead><tbody>
 <tr><td><font style="Tahoma" size="2"><font style="Arial" size="2">Get Device Information  - Fetch the device's runtime information.</font></td></td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Device Location Information - Fetch the device's current location. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Installed Applications - Fetch the device's installed application list. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Ring Device - Ring the device for the purpose of locating the device in case of misplacement.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Upload Device - Upload file to a specific folder on the device. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Download Device - Download file on a specific folder on the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Mute Device - Put the device in silent mode.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Change Lock Code - Changes the device's currently set lock code. From Android N upwards, clear passcode will not work.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Clear Password - Remove any password that the device owner has put. From Android N upwards, clear passcode will not work.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Send Notifications/Messages - Send a notification (message) to the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Enterprise Wipe - Wipe the entreprise portion of the device. </td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Wipe Device (Factory reset) - Factory reset a device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Device Lock (soft lock) - Lock the device remotely. Similar to pressing the power button on the device and locking it.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Reboot Device - Restart the phone for example for troubleshooting purposes.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Upgrade Firmware - Upgrade Android operating firmware ensuring that firmware and the device has to be compatible and only applicable in OEM scenarios.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Execute Shell Command - Remotely execute the shell commands on the device's command prompt.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Hard Lock - Lock a device remotely by an admin and only the admin can unlock the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">&nbsp;</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Manage Web Clip - Install a shortcut link to a web page/web app on the phone's home screen.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Trigger Google Play App - Install an app from the google play store.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Install/Uninstall/update applications - Capability to perform various application management tasks such as install, uninstall and update apps.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">View Device Screen - Screen sharing with the Admin.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Remote Control Device - Allow Admin to remotely control the device.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">&nbsp;</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Get Logcat - View the log of the operating system.</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Silent App Install - Install apps on the device without prompting the user to click install.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td></tr>
 <tr><td><font style="Tahoma" size="2">Remote Kiosk Enable - Enable or disable kiosk mode remotely for maintenance reasons, troubleshooting etc.</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td></tr>
</tbody></table>

## Policies

The <a target="_blank" href="{{< param doclink>}}key-concepts/operations-and-policies/">Policies</a> that can be applied on an Android device depends on the way the device is enrolled with the server. 

Accordingly, the table below indicates the policies applicable for each type of enrollment. 

<table class="tableizer-table">
    <thead>
        <tr class="tableizer-firstrow">
            <th>Feature</th>
            <th>Legacy</th>
            <th>Work Profile</th>
            <th>Dedicated / Fully Managed / System</th>
            <th>Fully managed (COPE)</th>
            <th>System app</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
                <a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/passcode-policy/"> Passcode Policy</a></td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td align="center"><font style="Tahoma" size="2">✓</td>
                    <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">Add a passcode strength policy to the device or to work profile</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"> <a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/encryption-settings/">Encryption Settings</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">Execute the encypt device storage.</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"> <a href="{{< param doclink>}}/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/wi-fi-settings/">Wi-Fi Settings</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">Push a configuration contaning the wifi profile of the company.</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/virtual-private-network/">Virtual Private network(VPN Settings)</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">Push a configuration contaning the VPN profile of the company.</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/work-profile-configurations/">Work-Profile Configurations</a></td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td><font style="Tahoma" size="2">Decides which system apps must be enabled or disabled in a work profile</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">COSU Profile Configuration</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Configure the behaviour of the Kiosk</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/application-restriction-settings/">Application Restriction Settings</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Decides which apps are allowed do be in a device</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/runtime-permission-policy/">Runtime permissions</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Permissions app require to work can be uptomatically granted and locked.</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/system-update-policy/">System Update Policy (COSU)</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Specify the strategy or the time windows to perform OS updates</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Monitor/Revoke Policies</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Continuously monitor the policies of the device to detect any policy violations.</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/certificate-install/}}">Certificate Install Settings</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Install certificate to devices remotely</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Global Proxy settings</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Reroute all the http communication of a device via a global http proxy</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/enrollment-application-install/">Enrollment app install</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Decides which apps needs to be installed upon enrollment</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Remote App configurations</a></td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Send the app configurations for user's installed apps</td>
        </tr>
        <tr>
            <td><font style="Tahoma" size="2"><a href="{{< param doclink>}}">Disallow removal of profile</a></td>
            <td align="center"><font style="Tahoma" size="2">✕</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td align="center"><font style="Tahoma" size="2">✓</td>
            <td><font style="Tahoma" size="2">Disable the user's ability to unenroll from EMM</td>
        </tr>
    </tbody>
</table>

###  Restrictions Policy 

<a target="_blank" href="{{< param doclink>}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/restrictions/">Restriction Policies</a>  are those that can be applied on a device restricting or controlling the use of certain specific device features. There are a large number of restrictions that can be applied on an Android device. 

The following table lists the available Restriction Policies for Android devices. 


<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th>Feature</th><th>Legacy</th><th>Work Profile</th><th>Dedicated / Fully Managed / System</th><th>Fully managed (COPE)</th><th>System app</th><th>Description</th></tr></thead><tbody>
 <tr><td><font style="Tahoma" size="2">Allow use of camera</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow the access to camera</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow modifying certificates</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow modifying installed certificates in the device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring VPN</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable configuring VPN settings</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring app control</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Hide the status bar of </td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow crossprofile copy paste</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Copying text between profiles is blocked</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow debugging</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable usb debuging</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow install apps </td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow installing apps to the device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow install from unknown sources</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow installing apps from unknown sources</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow modify accounts</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow modifying accounts such as Google, Facebook from being modified/ added/ removed</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow outgoing beams</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable using NFC to transfer data</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow location sharing</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable sharing device location</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow uninstall apps</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable uninstalling apps</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow parent profile app linking</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow apps in the personal profile to handle web links from the work profile</td></tr>
 <tr><td><font style="Tahoma" size="2">Ensure verifying apps</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Enforce only verified apps can be installed on the device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disable screen capture</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow capturing the screen of the device</td></tr>
 <tr><td><font style="Tahoma" size="2">Enable auto timing</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Enable or diable using time from mobile network as system time</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow  SMS</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable access to SMS</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow volume adjust</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable adjusting the volume of the device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow cell broadcast</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disables cell broadcasting messages of the network</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring bluetooth</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable configuring bluetooth settings</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring mobile networks</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable configuring moble network settings</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring tethering</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable configuring tethering settings</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow configuring WiFi</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable configuring WiFi settings</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow  safe boot</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow booting into safe mode</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow outgoing calls</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable outgoing calls</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow mount physical media</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable plugining in different media devices</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow create window</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable showing certain notifications, toasts and alert by apps</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow factory reset</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable factory resetting of devices</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow remove user</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow adding new users to device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow add user</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow removing users from device</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow network reset</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow user from performing network setting reset</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow USB file transfer</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow transfering data over USB</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow unmute microphone</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Configure access to microphone</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow status bar</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Block user from opening the notification bar and access to status bar</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow set wallpaper</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow changing wallpaper</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow auto fill</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow auto filling forms</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow bluetooth</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow bluetooth</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow bluetooth sharing</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disable sharing via bluetooth</td></tr>
 <tr><td><font style="Tahoma" size="2">Disallow data roaming</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✕</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td align="center"><font style="Tahoma" size="2">✓</td><td><font style="Tahoma" size="2">Disallow data roaming</td></tr>
</tbody></table>

